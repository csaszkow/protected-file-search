# -*- coding: utf-8 -*-
"""
Created on Wed Feb 14 08:22:39 2018

@author: Csaszarm
"""

import os
import glob
import shutil
import olefile
import zipfile
import pathlib
import datetime
from configparser import ConfigParser
from zipfile import BadZipfile

def readConfig():
    
    currentWorkingDirectory = os.getcwd()
    config = ConfigParser()
    config.read_file(open(os.path.join(currentWorkingDirectory, "config.txt")))
    
    rootDirectory = config.get("Paths", "RootDirectory")
    targetDirectory = config.get("Paths", "TargetDirectory")
    errorDirectory = config.get("Paths", "ErrorDirectory")
    logDirectory = config.get("Paths", "Logfile")
    
    if rootDirectory[-1].isalpha() or rootDirectory[-1].isdigit():
        rootDirectory = os.path.join(rootDirectory, "")
    if targetDirectory[-1].isalpha() or targetDirectory[-1].isdigit():
        targetDirectory = os.path.join(targetDirectory, "")
    if errorDirectory[-1].isalpha() or errorDirectory[-1].isdigit():
        errorDirectory = os.path.join(errorDirectory, "")
    if logDirectory[-1].isalpha() or logDirectory[-1].isdigit():
        logDirectory = os.path.join(logDirectory, "")
    
    return rootDirectory, targetDirectory, errorDirectory, logDirectory

def getopts(argv):
    opts = {}  
    while argv:  
        if argv[0][0] == '-':  
            opts[argv[0]] = argv[1]  
        argv = argv[1:]  
    return opts

def createLogFile(logFileDirectory):
    
    createLog = open(os.path.join(logFileDirectory, "logfile.txt"), "a")
    createLog.close()

    return createLog.name

def recordLog(logFile, file, target):
    
    timeStamp = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    
    log = open(logFile, "a")
    log.write(timeStamp + " : " + file + " COPIED to \n" + target + "\n")
    
    return

def recordLogRename(logFile, file, targetFullName):
    
    timeStamp = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    
    log = open(logFile, "a")
    log.write(timeStamp + " : " + file + " RENAMED AND COPIED to \n" + targetFullName + "\n")
    log.close()
    
    return

def createDirectory(targetDirectory):
    
    pathlib.Path(targetDirectory).mkdir(parents=True, exist_ok=True)
    
    return

def flagProtectedFiles():
    
    files = [x for x in glob.glob(rootDirectory + "**\*.*", recursive=True)]
    protectedFiles = []
    errorFiles = []
    unprotectedFiles = []

    for file in files:
        fileName, fileExtension = os.path.splitext(file)
        
        if fileExtension == ".docx" or fileExtension == ".xlsx":
            try:
                zf = zipfile.ZipFile(file)
                unprotectedFiles.append(file)
            except BadZipfile:
                protectedFiles.append(file)
    
        elif fileExtension == ".doc" or fileExtension == ".xls":
            try:
                ole = olefile.OleFileIO(file)
                meta = ole.get_metadata()
                ole.close()
                if meta.security == 1:
                    protectedFiles.append(file)
                elif meta.security == 0:
                    unprotectedFiles.append(file)
            except:
                errorFiles.append(file)
                
    return protectedFiles, errorFiles

def relocateFiles(target, files, logFile):

    for file in files:
        fileName = os.path.splitext(os.path.basename(file))[0]
        fileExtension = os.path.splitext(os.path.basename(file))[1]
        targetFullName = target + fileName + fileExtension
        isAlreadyExists = False
        
        while os.path.exists(targetFullName):
            isAlreadyExists = True
            fileName = os.path.splitext(os.path.basename(targetFullName))[0]
            fileExtension = os.path.splitext(os.path.basename(targetFullName))[1]
            autoNumber = fileName[-1]
            if autoNumber.isdigit():
                autoNumber = int(autoNumber) + 1
                targetFullName = target + fileName[:-1] + str(autoNumber) + fileExtension
            else:
                targetFullName = target + fileName + "_2" + fileExtension
        
        
        if not isAlreadyExists:
            recordLog(logFile, file, targetFullName)
        else:
            recordLogRename(logFile, file, targetFullName)
            
        shutil.copy(file, targetFullName)

if __name__ == "__main__":
    """
    import argparse
    
    currentWorkingDirectory = os.getcwd()
    defCurrDir = os.path.join(currentWorkingDirectory, "")
    defTargetDir = os.path.join(currentWorkingDirectory, "Protected", "")
    defErrorDir =  os.path.join(currentWorkingDirectory, "Error", "")  

    parser = argparse.ArgumentParser(description = "Jelszóval védett fájlok gyűjtése.")
    parser.add_argument("-r","--root", type = str, help = "A kiinduló mappa lokációja (kötelező).", required = False, default = defCurrDir)
    parser.add_argument("-t","--target", type = str, help = "A jelszóval védett fájlok gyűjtése ebbe a mappába.", required = False, default = defTargetDir)
    parser.add_argument("-e","--error", type = str, help = "A futás közben hibásan kezelt fájlok gyűjtése ebbe a mappába.", required = False, default = defErrorDir)

    arguments = vars(parser.parse_args())
    
    rootDirectory = arguments["root"]
    targetDirectory = arguments["target"]
    errorDirectory = arguments["error"]
    """
    
    rootDirectory, targetDirectory, errorDirectory, logFileDirectory = readConfig()
   
    createDirectory(targetDirectory)
    createDirectory(errorDirectory)
    createDirectory(logFileDirectory)
    logFile = createLogFile(logFileDirectory)
    
    protectedFiles, errorFiles = flagProtectedFiles()
    
    relocateFiles(targetDirectory, protectedFiles, logFile)
    relocateFiles(errorDirectory, errorFiles, logFile)